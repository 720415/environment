#!/bin/bash

set -euo pipefail

repo='dandavison/delta'
url="https://api.github.com/repos/${repo}/releases/latest"

raw_assets="$(curl -fsSL ${url})"
latest_assets=$(jq -c '[.assets[].browser_download_url]' <<< "${raw_assets}")

host_triple="$(rustc -vV | sed -rn 's%^host: (.*)$%\1%p')"
resource=$(
	jq -r --arg triple "${host_triple}" \
	'.[] | select(contains($triple) and endswith("gz"))' \
	<<< "${latest_assets}"
)

bin_dir="${XDG_BIN_HOME:-${HOME}/.local/bin}"
mkdir -p "${bin_dir}"
curl -fsSL "${resource}" | tar -xzC "${bin_dir}" --no-anchored --strip-components=1 'delta'

exit 0
