#!/bin/bash

set -euo pipefail

repo='sharkdp/hexyl'
url="https://api.github.com/repos/${repo}/releases/latest"

raw_assets="$(curl -fsSL ${url})"
latest_assets=$(jq -c '[.assets[].browser_download_url]' <<< "${raw_assets}")

host_triple="$(rustc -vV | sed -rn 's%^host: (.*)$%\1%p')"
resource=$(
	jq -r --arg triple "${host_triple}" \
	'.[] | select(contains($triple) and endswith("gz"))' \
	<<< "${latest_assets}"
)

tmp_file="$(mktemp)"
curl -fsSL "${resource}" > "${tmp_file}"

bin_dir="${XDG_BIN_HOME:-${HOME}/.local/bin}"
mkdir -p "${bin_dir}"
tar -xzC "${bin_dir}" --no-anchored --strip-components=1 'hexyl' < "${tmp_file}"

man_dir="${XDG_DATA_HOME:-${HOME}/.local/share}/man"
mkdir -p "${man_dir}/man1"
tar -xzC "${man_dir}/man1" --no-anchored --strip-components=1 'hexyl.1' < "${tmp_file}"

rm "${tmp_file}"

exit 0
