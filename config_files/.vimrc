" Should be located in:
" 	%HOMEPATH%\vimfiles\vimrc, or
" 	$HOME/.vim/vimrc

"see: https://devhints.io/vimscript for vim basics.
"see: https://vim.rtorr.com/ for more complex commands.

"Basic configuration {{{1
let g:sh_fold_enabled = 7
let g:zsh_fold_enable = 1
let g:netrw_liststyle = 3 "tree-view

scriptversion 4
set nocompatible
set viminfo="NONE"
set encoding=utf-8
set shortmess+=I
set cpoptions+=WI
set formatoptions+=jlp
set ttyfast
set lazyredraw
set ttimeoutlen=50
set updatetime=500

set diffopt+=vertical,algorithm:patience
set splitbelow
set splitright

set belloff=all
set t_vb=
set t_Co=256

set laststatus=2
set backspace=indent,start,eol
set whichwrap=

set hlsearch
set incsearch
set number
set scrolloff=5
set listchars=eol:$,tab:>-,trail:.
set iskeyword+=-

set tabstop=4
set shiftwidth=0
set softtabstop=-1
set autoindent
set smarttab
set smartindent

set matchpairs+=<:>
set matchtime=2
set showmatch

set colorcolumn=103
set foldcolumn=2
set signcolumn=number

set wildignore=*~,*.swp,*.o,*.a,*.so,*.out,*.pyc,*.pyo,*/.git/*,*/__pycache__/*
set wildmenu
set wildmode=longest:full,list:longest,full

set background=dark

"unicode characters MUST be in a character class when in a pattern.
let s:whitespace_chars = ' \u00a0\u1680\u180e\u2000-\u200b\u202f\u205f\u3000\ufeff'
" let s:eol_whitespace_pattern = '[\t' .. s:whitespace_chars .. ']\+$'
let s:eol_whitespace_pattern = '[' .. s:whitespace_chars .. ']\+$'
" show lines ending in tabs, but are not only tabs.
let s:eol_whitespace_pattern ..= '\|[^\t]\+\zs\t\+$'
let s:strip_whitespace_pattern = s:eol_whitespace_pattern
" show spaces that precede tabs, but don't add them to the strip pattern.
let s:eol_whitespace_pattern ..= '\|[' .. s:whitespace_chars .. ']\+\ze\t'
let s:strip_whitelines_pattern = '[\t' .. s:whitespace_chars .. ']\+\%$'
"}}}1

"Helper functions {{{1
function! s:ActionPrompt(prompt, completion = '')
	call inputsave()
	if a:completion !=# ''
		execute a:prompt .. input(a:prompt, '', a:completion)
	else
		execute a:prompt .. input(a:prompt, '')
	endif
	call inputrestore()
endfunction

function! s:Windows()
	return has('win64') || has('win32unix') || has('win32')
endfunction

execute 'language en_US' .. (s:Windows() ? '' : '.UTF-8')

function! s:VimDir()
	return expand(s:Windows() ? '~/vimfiles' : '~/.vim')
endfunction

function! s:ToggleMouseSupport()
	if &mouse ==# 'a'
		set mouse=
	else
		set mouse=a
	endif
	echo printf('Mouse: %s', (&mouse ==# '') ? 'Off' : 'On')
endfunction

func EatChar()
	let l:c = nr2char(getchar(0), v:true)
	return (l:c =~ ' ') ? '' : l:c
endfunc

function! s:ClearTrailingWhitespace()
	syntax clear TrailingWhitespace
endfunction

function! s:RefreshTrailingWhitespace()
	call <SID>ClearTrailingWhitespace()
	execute printf('syntax match TrailingWhitespace excludenl "%s"', s:eol_whitespace_pattern)
endfunction

function! s:RefreshSession()
	call <SID>RefreshTrailingWhitespace()
	call lightline#toggle()
	call lightline#toggle()
endfunction

function! GitBranch() "this cannot be script-local
	let l:branch = gitbranch#name()
	return ('' ==# l:branch ? '' : printf('(%s)', l:branch))
endfunction

function! GitStatus() "this cannot be script-local
	let [l:a, l:m, l:d] = GitGutterGetHunkSummary()
	return printf('+%d ~%d -%d', l:a, l:m, l:d)
endfunction

function! s:GotoTrailingWhitespace(search_backwards, from, to)
	" Save the current search
	let l:prev_search = @/
	let l:cur_line = line('.')
	let l:cur_col = col('.')
	
	" Move to start of range (if we are outside of it)
	if l:cur_line < a:from || l:cur_line > a:to
		if a:search_backwards !=# v:false
			call cursor(a:to, 0)
			call cursor(0, col('$'))
		else
			call cursor(a:from, 1)
		endif
	endif
	
	" Set options (search direction, last searched line)
	let l:opts = 'wz'
	let l:until = a:to
	if a:search_backwards !=# v:false
		let l:opts ..= 'b'
		let l:until = a:from
	endif
	" Full file, allow wrapping
	if a:from ==# 1 && a:to ==# line('$')
		let l:until = 0
	endif
	
	" Go to pattern
	let l:found = search(s:eol_whitespace_pattern, l:opts, l:until)
	
	" Restore position if there is no match (in case we moved it)
	if l:found ==# 0
		call cursor(l:cur_line, l:cur_col)
	endif
	
	" Restore the saved search
	let @/ = l:prev_search
endfunction

function! s:StripWhitespace(line1, line2)
	let l:search_index = histnr('search')
	let l:prev_search = @/
	let l:cur_line = line('.')
	let l:cur_col = col('.')
	
	silent execute printf(':%s,%ss/%s//e', a:line1, a:line2, s:strip_whitespace_pattern)
	
	if a:line2 >=# line('$')
		silent execute printf('%%s/%s//e', s:strip_whitelines_pattern)
	endif
	
	while histnr('search') ># max([l:search_index, 0])
		call histdel('search', -1)
	endwhile
	" let @/ = histget('search', -1)
	let @/ = l:prev_search
	call cursor(l:cur_line, l:cur_col)
endfunction

function! s:StripWhitespaceCommand(line1, line2, force)
	if &readonly && a:force ==# 0
		echoerr "E45: 'readonly' option is set (add ! to override)"
	else
		call <SID>StripWhitespace(a:line1, a:line2)
	endif
endfunction

function! s:SetupWhitespaceCommands()
	call <SID>RefreshTrailingWhitespace()
	let b:strip_whitespace_on_save = get(b:, 'strip_whitespace_on_save', &modifiable)
	augroup trailingwhitespace
		autocmd!
		autocmd InsertLeave,BufReadPost * call <SID>RefreshTrailingWhitespace()
		autocmd BufWinLeave * if expand("<afile>") ==# expand('%') | call <SID>ClearTrailingWhitespace() | endif
		if b:strip_whitespace_on_save ==# 1
			autocmd BufWritePre * call <SID>StripWhitespace(1, line('$'))
		endif
	augroup END
endfunction

function! s:ToggleStripWhitespaceOnSave()
	let b:strip_whitespace_on_save = v:true - get(b:, 'strip_whitespace_on_save', &modifiable)
	echo printf('Strip trailing WS on save: %s', (b:strip_whitespace_on_save ==# 0) ? 'Off' : 'On')
	call <SID>SetupWhitespaceCommands()
endfunction
"}}}1

"Pre-Plug configuration {{{1
if has('gui_running')
	set guifont=Cascadia_Mono_NF:h12,SauceCodePro_Nerd_Font_Mono:h11,Source_Code_Pro:h11,Consolas:h12
endif

if $TERM_PROGRAM !=# 'Apple_Terminal' && has('termguicolors')
	set termguicolors
endif

let g:plug_threads = 32

"see: ':h g:lightline.component' for details; default values apply if not listed here.
let g:lightline = {
\	'colorscheme': 'deus',
\	'component': {
\		'inactive': 'INACTIVE',
\	},
\	'component_function': {
\		'gitbranch': 'GitBranch',
\		'gitstatus': 'GitStatus',
\	},
\	'active': {
\		'left': [
\			[ 'mode', 'paste', ],
\			[ 'readonly', 'filename', 'gitstatus', 'modified', ],
\		],
\		'right': [
\			[ 'lineinfo', ],
\			[ 'percent', ],
\			[ 'gitbranch', 'filetype', ],
\		],
\	},
\	'inactive': {
\		'left': [
\			[ 'inactive', ],
\			[ 'readonly', 'filename', 'gitstatus', 'modified', ],
\		],
\		'right': [
\			[ 'lineinfo', ],
\			[ 'percent', ],
\			[ 'gitbranch', 'filetype', ],
\		],
\	},
\}

"g:python_highlight_all enables too much.
let g:python_highlight_builtins = 1
let g:python_highlight_builtin_funcs_kwarg = 1
let g:python_highlight_string_format = 1
let g:python_highlight_indent_errors = 1
let g:python_highlight_class_vars = 1
let g:python_highlight_file_headers_as_comments = 1

let g:mergetool_layout = 'lr,m'
" let g:mergetool_prefer_revision = 'remote'

let g:gitgutter_map_keys = 0
"<Plug> mappings need to be made before loading the pluigins.
nmap [h <Plug>(GitGutterPrevHunk)
nmap ]h <Plug>(GitGutterNextHunk)
nmap ghs <Plug>(GitGutterStageHunk)
nmap ghp <Plug>(GitGutterPreviewHunk)
nmap ghu <Plug>(GitGutterUndoHunk)

let g:tagbar_autoclose = 1
let g:tagbar_autofocus = 1
let g:tagbar_width = max([50, winwidth(0) / 4])
let g:tagbar_show_linenumbers = 1
"let g:tagbar_show_tag_linenumbers = 1
let g:tagbar_scrolloff = 5

"only use this if not using a completion plugin.
let g:ale_completion_enabled = 1
" let g:ale_fixers = { 'python': ['ruff'], }
let g:ale_linters = {
\	'rust': ['analyzer'],
\	'python': ['mypy', 'pylint', 'pylsp', 'ruff', 'unimport', 'yapf'],
\}
"}}}1

"Plug setup {{{1
let s:plugfile = printf('%s/autoload/plug.vim', s:VimDir())
if !filereadable(s:plugfile) && v:true
	let s:plugurl = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	let s:plug_dl_silent = s:Windows() ? '' : ' &>/dev/null'
	execute 'silent' printf('!curl --create-dirs -fL -o %s %s%s', s:plugfile, s:plugurl, s:plug_dl_silent)
	autocmd VimEnter * ++once PlugInstall --sync | source $MYVIMRC
endif
try
	call plug#begin(printf('%s/vimplugs', s:VimDir()))
	"only LOAD the plug-ins here; configure them before plug#begin
	
	"use PlugUpgrade to update Plug itself (in autoload).
	Plug 'junegunn/vim-plug' "this is mainly to get the documentation
	
	Plug 'ypcrts/securemodelines'
	Plug 'ConradIrwin/vim-bracketed-paste' "avoid issues when pasting in a terminal; no help page.
	Plug 'ap/vim-css-color' "makes the bg of a color literal that color; no help page.
	Plug 'itchyny/vim-gitbranch'
	Plug 'itchyny/lightline.vim'
	Plug 'joshdick/onedark.vim' "see website for configuration, if needed
	
	"syntax highlighting for git files; no useful help
	Plug 'tpope/vim-git', { 'for': ['git', 'gitcommit', 'gitconfig', 'gitrebase'] }
	Plug 'chrisbra/csv.vim', { 'for': 'csv' } "see: ':h csv-toc'
	Plug 'raimon49/requirements.txt.vim', { 'for': 'requirements' }
	Plug 'vim-python/python-syntax', { 'for': 'python' }
	Plug 'rust-lang/rust.vim', { 'for': 'rust' }
	Plug 'tmux-plugins/vim-tmux', { 'for': 'tmux' }
	Plug 'dylon/vim-antlr', { 'for': 'antlr4' }
	"Plug 'digitaltoad/vim-pug', { 'for': ['pug', 'jade'] }
	
	Plug 'tpope/vim-commentary'
	Plug 'tpope/vim-surround'
	Plug 'tpope/vim-repeat' "enable . to repeat more complex commands; no help page.
	Plug 'vim-utils/vim-husk' "enable tmux-yank to grab lines through vim; see: ':h husk'
	Plug 'bronson/vim-visual-star-search' "enable * to search for Visual selection; no help page
	Plug 'samoshkin/vim-mergetool' "enables vim as decent git mergetool; see website for details
	Plug 'tmhedberg/simpylfold', { 'for': 'python' }
	Plug 'airblade/vim-gitgutter' "see: ':h gitgutter'
	Plug 'ton/vim-alternate' "swap between header and source files in C/C++
	Plug 'flwyd/vim-conjoin' "see: ':h conjoin'
	Plug 'preservim/tagbar' "see: ':h tagbar'
	
	Plug 'dense-analysis/ale' "see: ':h ale'; MUCH configuration TODO.
	call plug#end()
	
	set noshowmode
	
	nnoremap <Leader><F11> :PlugUpdate<CR>
	
	colorscheme onedark
	highlight Cursor gui=reverse guifg=NONE guibg=NONE
	
	nnoremap <Leader>g :GitGutterToggle<CR>
	nnoremap gf :GitGutterFold<CR>
	autocmd BufWritePost * GitGutter
	
	nnoremap <silent> <F4> :Alternate<CR>
	nnoremap <silent> <F2> :TagbarToggle<CR>
	
	set omnifunc=ale#completion#OmniFunc
	"~TODO: see `':map' | grep -i ale` for available bindings
	
catch /^Vim(.*):E\%(117\|185\)/
	"just discard the error, Plug isn't available;
	"alternative configuration below.
	"echoerr v:exception
	
	colorscheme slate
	highlight PreProc guifg=indianred guibg=grey15
	
	set statusline= "Clear any previous content
	set statusline+=%y\ %f\ %m%r
	set statusline+=%= "Padding to right side
	set statusline+=[col\ %c]\ (%l\ of\ %L)\ {%n}
	
	syntax on
	filetype plugin indent on
endtry
"}}}1

"Setup keybindings {{{1
nnoremap <silent> <F1> :vertical help function-list<CR>
nnoremap <C-F2> :!ctags -R .<CR>
nnoremap <silent> <F3> :set list!<CR>
"this will effectively reload the current file
nnoremap <silent> <F5> :edit<CR>
nnoremap <silent> <F6> :set spell!<CR>
nnoremap <silent> <F8> :set cursorcolumn!<CR>
nnoremap <silent> <C-F8> :set cursorline!<CR>
nnoremap <C-F11> :call <SID>ActionPrompt('set colorcolumn=')<CR>
"this one is a wacky script; don't add <Leader> here
nnoremap <silent> <C-F10> :source $VIMRUNTIME/syntax/hitest.vim<CR>
nnoremap <silent> <F12> :set relativenumber!<CR>

nnoremap :Q :quit
nnoremap Y y$
nnoremap <BS> X

nnoremap <silent> <Leader><F2> :call <SID>ToggleMouseSupport()<CR>
nnoremap <silent> <Leader><F3> :call <SID>ToggleStripWhitespaceOnSave()<CR>
nnoremap <silent> <Leader><F5> :source $MYVIMRC <Bar> :call <SID>RefreshSession()<CR>
nnoremap <Leader><F6> :set fileformat=unix<CR>
"format current file with configured style
nnoremap <Leader><F7> gg=G<C-o><C-o>
nnoremap <Leader><F9> :edit $MYVIMRC<CR>
nnoremap <Leader>= <C-w>=

nnoremap <Leader>e :Sexplore!<CR>
nnoremap <Leader>E :execute ':Sexplore! ' .. getcwd()<CR>

nnoremap <Leader>s :call <SID>ActionPrompt('set filetype=', 'syntax')<CR>

nnoremap <Leader>r zR
nnoremap <Leader>h <C-w>h
nnoremap <Leader>j <C-w>j
nnoremap <Leader>k <C-w>k
nnoremap <Leader>l <C-w>l
nnoremap <Leader>o <C-w>o
nnoremap <Leader>w :write<CR>
nnoremap <Leader><Leader> :write<CR>
nnoremap <Leader>q :quit<CR>
nnoremap <Leader>z :call <SID>ActionPrompt('set foldmethod=')<CR>
nnoremap <Leader>x :exit<CR>
nnoremap <Leader>c :bdelete<CR>
nnoremap <Leader>b :buffers<CR>
nnoremap <Leader>m zM
nnoremap <silent> <Leader>] :nohlsearch<CR>
nnoremap <silent> <Leader>/ :let @/ = ''<CR>

if s:Windows()
	nnoremap <C-s> :write<CR>
endif

"resize windows with arrows
nnoremap <silent> <C-Up> :resize +2<CR>
nnoremap <silent> <C-Down> :resize -2<CR>
nnoremap <silent> <C-Left> :vertical resize -2<CR>
nnoremap <silent> <C-Right> :vertical resize +2<CR>

"center the cursor vertically on move
nnoremap <silent> j jzz
nnoremap <silent> k kzz

nnoremap <silent> [w :PrevTrailingWhitespace<CR>
nnoremap <silent> ]w :NextTrailingWhitespace<CR>

nnoremap <C-k> :bprevious<CR>
nnoremap <C-j> :bnext<CR>

"make S-Insert paste the contents of the clipboard
noremap! <silent> <S-Insert> <C-r>+

"change S-Arrows to enter visual mode
nnoremap <S-Up> V<Up>
nnoremap <S-Down> V<Down>
nnoremap <S-Left> v<Left>
nnoremap <S-Right> v<Right>

"remove paging from S-Arrows
vnoremap <S-Up> <Up>
vnoremap <S-Down> <Down>
vnoremap <S-Left> <Left>
vnoremap <S-Right> <Right>

inoremap <S-Up> <Up>
inoremap <S-Down> <Down>
inoremap <S-Left> <Left>
inoremap <S-Right> <Right>

"drag a visual group of lines up or down
vnoremap J :move '>+1<CR>gv-gv
vnoremap K :move '<-2<CR>gv-gv

"retain copied text after pasting in visual mode
vnoremap p "_dP

"maintain visual mode after shifting
vnoremap < <gv
vnoremap > >gv

tnoremap <Leader>; <C-w>N
tnoremap <Leader>: <C-w>:
tnoremap <Leader>c <C-w><C-c>
"}}}1

"Abbreviations {{{1
inoreabbrev <silent> cmain int main(int const argc, char const *const argv[]) {<CR><CR>return 0;<CR>}<Up><Up><Tab><C-R>=EatChar()<CR>
"inoreabbrev <silent> pymain def main():<CR><Tab><C-R>=EatChar()<CR>
"inoreabbrev <silent> rsmain fn main() {<CR><CR>}<Up><Tab><C-R>=EatChar()<CR>
"}}}1

"Manual commands {{{1
"see: https://github.com/ntpeters/vim-better-whitespace for details
highlight TrailingWhitespace ctermbg=red guibg=#FF0000
command! -range=% PrevTrailingWhitespace call <SID>GotoTrailingWhitespace(v:true, <line1>, <line2>)
command! -range=% NextTrailingWhitespace call <SID>GotoTrailingWhitespace(v:false, <line1>, <line2>)
command! -bang -range=% StripWhitespace call <SID>StripWhitespaceCommand(<line1>, <line2>, <bang>0)
command! ToggleStripWhitespaceOnSave call <SID>ToggleStripWhitespaceOnSave()
"}}}1

"Auto commands {{{1
autocmd TerminalWinOpen * wincmd H
autocmd WinEnter,BufWinEnter * call <SID>SetupWhitespaceCommands()

augroup extensionsettings
	autocmd!
	"# vim: set ts=4 sw=0 sts=-1 noet ai:
	autocmd FileType python setlocal ts=4 sw=0 sts=-1 noet ai cpo+=I
	autocmd FileType rust setlocal ts=4 sw=0 sts=-1 noet ai cpo+=I
	autocmd FileType c,cpp setlocal fdm=syntax
	autocmd FileType yaml setlocal ts=2 sw=0 sts=-1 et ai
	autocmd FileType vim setlocal fdm=marker
	autocmd FileType pug,jade setlocal ts=2 sw=0 sts=-1 et ai cpo+=I
	autocmd FileType json syntax match Comment +\/\/.\+$+
	autocmd BufRead,BufNewFile *.g4 set filetype=antlr4
augroup END
"}}}1

" vim: set fdm=marker:
