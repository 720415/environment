#!/usr/bin/bash

# Should be placed in:
# 	%HOMEPATH%\.bashrc, or
# 	$HOME/.bashrc

# See: https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html for help.

shopt -s autocd
shopt -s histappend

export -n GCC_EXEC_PREFIX=
export -n GIT_EXEC_PATH=
export -n LD_LIBRARY_PATH=
export -n LD_PRELOAD=

locale_pref=en_US.UTF-8
export LC_ALL="${locale_pref}"
export LANG="${locale_pref}"
export LANGUAGE="${locale_pref}"
export -n locale_pref=

HISTFILE="${HOME}/.bash_history"
HISTSIZE=999999999
export PAGER='less -+X -iRx4'
export EDITOR='vim'

eval "$(dircolors -b)"

export PYTHONUTF8=1
export USE_EMOJI=0
export GHCUP_USE_XDG_DIRS=1
export BAT_THEME='OneHalfDark'
export RICH_THEME='one-dark'
export BAT_PAGER="${PAGER}"
export DELTA_PAGER="${PAGER}"
export JQ_COLORS='1;30:0;33:0;33:0;37:0;32:1;36:1;35:1;34'

VIRTUAL_ENV_DISABLE_PROMPT=1

GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
GIT_PS1_SHOWUPSTREAM='auto'
GIT_PS1_SHOWCONFLICTSTATE='yes'
GIT_PS1_DESCRIBE_STYLE='branch'

export PATH="${HOME}/${venvs_dir:-venvs}/global/Scripts:${PATH}"

alias eb="\${EDITOR} ${HOME}/.bashrc"
alias sb="source ${HOME}/.bashrc"
alias ls='ls --color=auto -Fh'
alias ll='ls -l'
alias la='ll -av'
alias l.='ll -d .*'
alias l,='ll -dr .. .'
alias less="${PAGER}"

alias dr='diff -bur'
alias gar='git apply --reject'
alias gbv='git branch -vv'
alias gca='git commit --amend -C HEAD'
alias gclean='git clean -Xdf'
alias gco='git checkout'
alias gdh='git diff HEAD'
alias gds='git diff --staged'
alias gdu='git diff'
alias gfa='git fetch --all'
alias gp='git pull --stat'
alias gpr='git pull --stat --rebase'
alias gr='git remote -v'
alias gra='git rebase --abort'
alias grc='git rebase --continue'
alias gri='git rebase --interactive'
alias gs='git status --short'
alias gsi='git status --short --ignored'
alias gsl='git status'
alias gsli='git status --ignored'

# Intentionally not including a target tree-ish.
alias grh='git reset --hard'
alias grm='git reset --mixed'
alias grs='git reset --soft'

alias cls='clear'
alias uncolor="sed 's%\x1B\[[0-9;]*[mK]%%g'"
alias whence='which'

function which() {
	(declare -f; alias) | command which --read-alias --read-functions --show-dot --show-tilde "${@}" 2>/dev/null
}

if [[ -n "$(whence vim)" ]]; then
	alias vimr='vim -R'
fi

if [[ -n "$(whence ptpython)" ]]; then
	alias python="clear; ptpython"
else
	alias python="clear; python -q"
fi
alias py='python'
alias pi='python -i'
alias pydoc='python -m pydoc'

if [[ -n "$(whence radon)" ]]; then
	alias radon="radon cc -sao LINES --no-assert --show-closures"
fi

if [[ -n "$(whence yapf)" ]]; then
	alias yapf="yapf --diff"
fi

if [[ -n "$(whence bat)" ]]; then
	alias cat="bat --pager=never"
fi

if [[ -n "$(whence rg)" ]]; then
	alias rg='rg --engine=auto'
	alias grep='rg'
else
	alias grep="grep --color=auto --exclude-dir='.git' -IPn"
fi

function gaa() {
	git add --all
	git status --ignored --short
}

function gia() {
	# The following expansion is intentionally unquoted.
	git add --intent-to-add $(git ls-files --others --exclude-standard)
}

function gcl() {
	local has_changes=0
	if [[ 1 -lt "$(git status --ignored --short | command grep -v '^!' | wc -l)" ]]; then
		has_changes=1
		git stash push --include-untracked
	fi
	git clean -xdf
	if [[ 0 -ne "${has_changes}" ]]; then
		git stash pop
	fi
}

function gg() {
	git log --all --decorate --oneline --graph --color=auto -n 50 "${@}"
}

function gl() {
	git log --decorate --decorate-refs-exclude=refs/remotes/origin/HEAD --oneline --graph --color=auto -n 10 "${@}"
}

function gscs() {
	if [[ 1 -ne ${#} ]]; then
		echo "usage: ${FUNCNAME[0]} GPG_FINGERPRINT" >&2
		echo "You need to provide a GPG fingerprint to sign commits." >&2
		return 1
	fi
	
	git rev-parse --absolute-git-dir &>/dev/null
	if [[ 0 -eq ${?} ]]; then
		git config commit.gpgsign true
		git config user.signingkey "${1}"
	fi
}

function gsh() {
	local ref
	if [[ -n "$(command grep -P '^[1-9]\d*$' <<< ${1:-})" ]]; then
		ref="${1}"
		shift
	fi
	git show --show-signature ${@:+"${@}"} "HEAD${ref:+~${ref}}"
}

function reload_gpg_agent() {
	gpgconf --reload gpg-agent
}

function spp() {
	tr ':' '\n' <<< "${!1:-${PATH}}"
}

function z() {
	if [[ -z "$(whence zsh)" ]]; then
		echo "\n\tZSH is not available..."
		return
	fi
	
	local -a vars_to_keep=()
	vars_to_keep+=('DBUS_SESSION_BUS_ADDRESS')
	# vars_to_keep+=('KRB5CCNAME')
	vars_to_keep+=('SHELL')
	vars_to_keep+=('SSH_CLIENT')
	vars_to_keep+=('SSH_CONNECTION')
	vars_to_keep+=('SSH_TTY')
	vars_to_keep+=('USER')
	vars_to_keep+=('XDG_RUNTIME_DIR')
	vars_to_keep+=('XDG_SESSION_ID')
	
	local -a to_keep=()
	for var in ${vars_to_keep[@]}; do
		declare -p "${var}" &>/dev/null
		if [[ 0 -eq ${?} ]]; then
			to_keep+=("${var}=${!var}")
		fi
	done
	
	exec env -i "${to_keep[@]}" 'TERM=xterm-256color' zsh
}

if [[ -n "$(whence alacritty)" ]]; then
	function ao() {
		# Open Alacritty in the given directory, then disown it.
		nohup alacritty --working-directory "${1:-$(pwd)}" &>/dev/null & disown
	}
fi

function check_notices(){
	RETC=${?}
	
	# TODO: This doesn't seem to work.
	# Maybe it has to do with how G4W reports paths with spaces and backslashes.
	local home=$(readlink -m "${HOME}")
	if [[ $(readlink -m "${PWD}") =~ "^${home//./\.}" ]]; then
		P_PWD=$'\e[38;5;243m'
	else
		P_PWD=$'\e[38;5;209m'
	fi
	
	local jobs="$(jobs | wc -l)"
	if [[ 1 -le "${jobs}" ]]; then
		P_JOBS=" jobs=${jobs}"
	else
		P_JOBS=''
	fi
	
	if [[ 0 -eq "${RETC}" ]]; then
		P_RETC=$'\e[38;5;034m'
	else
		P_RETC=$'\e[38;5;124m'
	fi
}

if [[ -z "$(whence __git_ps1)" ]]; then
	function __git_ps1() { return 0; }
fi

PROMPT_COMMAND='check_notices'

P_INFO=$'\e[38;5;178m'
P_MACH=$'\e[38;5;035m'
P_SEPR=$'\e[38;5;075m'
P_REPO=$'\e[38;5;128m'
P_VENV=$'\e[38;5;068m'
P_JOBC=$'\e[38;5;130m'
P_INIT=$'\e[38;5;174m'

NEWLINE=$'\n'
RESET=$'\e[0m'
PS1=''
PS1+='${NEWLINE}'
PS1+='\[${P_INFO}\]['
PS1+='\[${P_MACH}\]\h'
PS1+='\[${P_SEPR}\]:'
PS1+='\[${P_PWD:-}\]\w'
PS1+='\[${P_REPO}\]$(__git_ps1 " (%s)")'
PS1+='\[${P_INFO}\]]'
PS1+='\[${P_VENV}\]${VIRTUAL_ENV:+ venv}'
PS1+='\[${P_JOBC}\]${P_JOBS:-}'
PS1+=' \[${P_RETC:-}\]${RETC:-}'
PS1+='${NEWLINE}'
PS1+='\[${P_INIT}\]\$ '
PS1+='\[${RESET}\]'

# vim: set ts=4 sw=0 sts=-1 noet ai ft=bash:
