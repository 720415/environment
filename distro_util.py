# This is not a script.

'''Utilities to examine an OS distro.'''

# mypy: disable-error-code="unreachable"

from enum import Enum, auto, nonmember, unique
from functools import cache, cached_property
from getpass import getuser
from json import loads
from os import environ
from os.path import normpath
from pathlib import Path
from platform import system
from shutil import which
from subprocess import run
from typing import Self

__all__ = [
	'distro_name',
	'distro_like',
	'DistroFamily',
	'InitFamily',
	'SessionFamily',
	'LibcFamily',
]

def _fetch_host_info() -> dict[str, str]:
	'''Get the host distro name and family.'''
	
	if (host_system := system()) != 'Linux':
		return {'ID': host_system.lower(), 'ID_LIKE': ''}
	
	from platform import freedesktop_os_release
	
	linux_info = freedesktop_os_release()
	return {
		'ID': linux_info['ID'],
		'ID_LIKE': linux_info.get('ID_LIKE', ''),
	}

RELEASE_INFO = _fetch_host_info()

def distro_name() -> str:
	'''Get a value representing the distribution name.'''
	
	return RELEASE_INFO['ID']

def distro_like() -> str:
	'''Get a value representing the distribution this one is like.'''
	
	return RELEASE_INFO['ID_LIKE']

@unique
class DistroFamily(Enum):
	'''Enumeration of supported distributions.'''
	
	UNKNOWN = auto()
	MACOS = auto()
	ALPINE = auto()
	DEBIAN = auto()
	REDHAT = auto()

	@staticmethod
	def __get_script_dir() -> Path:
		'''Get the path of the containing directory.'''
		
		path = Path(__file__)
		if path.is_symlink():
			path = path.readlink()
		return Path(normpath(str(path.parent)))
	
	@nonmember
	class UnknownDistroError(Exception):
		'''An Exception to represent an unknown distribution.'''
		
		def __init__(self, distro: str, /, message: str = 'Unknown distro') -> None:
			'''Construct an Exception with the stored Distro.'''
			
			self._distro = distro
			super().__init__(message)
		
		def __str__(self, /) -> str:
			'''Get a string representation of this Exception.'''
			
			return f"{super().__str__()}: '{self._distro}'"
	
	@classmethod
	@cache
	def fetch(cls, /) -> Self:
		'''Get a value representing the distribution family.'''
		
		d_name = distro_name()
		ret = cls.UNKNOWN
		
		for t_name in (distro_like(), d_name):
			if ret is cls.UNKNOWN:
				if t_name in ('darwin', ):
					ret = cls.MACOS
				
				elif t_name in ('alpine', ):
					ret = cls.ALPINE
				
				elif t_name in ('debian', 'raspbian', 'ubuntu'):
					ret = cls.DEBIAN
				
				elif t_name in ('fedora', ):
					ret = cls.REDHAT
		
		# Windows is _not_ supported here, but should also not cause an error.
		if ret is cls.UNKNOWN and not ret.is_windows():
			raise cls.UnknownDistroError(d_name) # type: ignore[operator]
		
		return ret # type: ignore[return-value]
	
	@classmethod
	@cache
	def homebrew_command(cls, /) -> list[str]:
		'''Get the command needed to run Homebrew with the correct permissions.'''
		
		return [str(cls.__get_script_dir() / 'macos' / 'brew_wrapper.sh')]
	
	def is_windows(self, /) -> bool:
		'''Check if the current system is a Windows system.'''
		
		return self == self.__class__.UNKNOWN and distro_name() == 'windows'
	
	@cached_property
	def pkg_management(self, /) -> tuple[str, str]:
		'''Get the package type and package manager for this family.'''
		
		# pylint: disable=too-many-return-statements(R0911)
		
		if self == self.MACOS:
			# The following are other meaningful mappings that are only used in special circumstances:
			# ('app', 'mas') # '.app's are managed by the App Store (via `mas`) or manually,
			# ('pkg', 'installer') # '.pkg's are managed by `installer`, and rarely seen w/o a gui.
			return ('', 'brew')
		
		if self == self.ALPINE:
			return ('apk', 'apk')
		
		if self == self.DEBIAN:
			return ('deb', 'apt')
		
		if self == self.REDHAT:
			return ('rpm', 'dnf')
		
		return ('', '')
	
	@cached_property
	def install_steps(self, /) -> tuple[list[str], list[str], str | None]:
		'''Get steps to install system packages.'''
		
		base_cmd = [self.pkg_management[1]]
		steps = []
		distro_input = None
		
		if self == self.MACOS:
			base_cmd = self.homebrew_command()
			steps = ['update --quiet', 'install --quiet --formula']
		
		elif self == self.ALPINE:
			steps = ['update', 'add']
		
		elif self in (self.DEBIAN, self.REDHAT):
			base_cmd.extend(['-y'])
			steps = ['update', 'install']
		
		return (base_cmd, steps, distro_input)
	
	@cached_property
	def upgrade_steps(self, /) -> tuple[list[str], list[str]]:
		'''Get the steps to upgrade system packages.'''
		
		base_cmd = [self.pkg_management[1]]
		steps = ['']
		
		if self == self.MACOS:
			base_cmd = [*self.homebrew_command(), 'upgrade', '--quiet']
		
		elif self == self.ALPINE:
			steps = ['update', 'upgrade']
		
		elif self == self.DEBIAN:
			base_cmd.extend(['-y'])
			steps = ['update', 'full-upgrade', 'autoremove']
		
		elif self == self.REDHAT:
			base_cmd.extend(['-y'])
			steps = ['upgrade', 'autoremove']
		
		return (base_cmd, steps)

@unique
class InitFamily(Enum):
	'''Enumeration of `init` families.'''
	
	UNKNOWN = auto()
	LAUNCHD = auto()
	SYSTEMD = auto()
	OPENRC = auto()
	RUNIT = auto()
	
	@nonmember
	class UnknownInitSystemError(Exception):
		'''An Exception to represent an unknown init system.'''
		
		def __init__(self, /, message: str = 'Unknown init system') -> None:
			'''Construct an Exception with a useful message.'''
			
			super().__init__(message)
	
	@classmethod
	@cache # There can only be one active init system.
	def fetch(cls, /) -> Self:
		'''Get a value representing the init system family.'''
		
		ret = cls.UNKNOWN
		
		# This list is ordered in descending likelihood of being the active init system.
		
		if which('launchctl') is not None:
			ret = cls.LAUNCHD
		
		elif which('systemctl') is not None:
			ret = cls.SYSTEMD
		
		elif which('rc-service') is not None:
			ret = cls.OPENRC
		
		elif which('sv') is not None:
			ret = cls.RUNIT
		
		if ret is cls.UNKNOWN:
			raise cls.UnknownInitSystemError # type: ignore[misc]
		
		return ret # type: ignore[return-value]
	
	@cache
	def get_enable_service_cmd(self, service_name: str, /) -> list[str]:
		'''Generate the command to enable a service on startup.'''
		
		if self == self.LAUNCHD:
			return ['launchctl', 'enable', f"system/{service_name}"]
		
		if self == self.SYSTEMD:
			return ['systemctl', 'enable', service_name]
		
		if self == self.OPENRC:
			return ['rc-update', 'add', service_name, 'default']
		
		if self == self.RUNIT:
			return ['ln', '-sf', f"/etc/sv/{service_name}", '/var/service/']
		
		return []
	
	@cache
	def get_disable_service_cmd(self, service_name: str, /) -> list[str]:
		'''Generate the command to disable a service on startup.'''
		
		if self == self.LAUNCHD:
			return ['launchctl', 'disable', f"system/{service_name}"]
		
		if self == self.SYSTEMD:
			return ['systemctl', 'disable', service_name]
		
		if self == self.OPENRC:
			return ['rc-update', 'del', service_name, 'default']
		
		if self == self.RUNIT:
			# return ['rm', f"/var/service/{service_name}"]
			return ['touch', f"/etc/sv/{service_name}/down"]
		
		return []
	
	@cache
	def get_restart_service_cmd(self, service_name: str, /) -> list[str]:
		'''Generate the command to (re-)start a service.'''
		
		if self == self.LAUNCHD:
			return ['launchctl', 'kickstart', '-k', f"system/{service_name}"]
		
		if self == self.SYSTEMD:
			return ['systemctl', 'restart', service_name]
		
		if self == self.OPENRC:
			return ['rc-service', service_name, 'restart']
		
		if self == self.RUNIT:
			return ['sv', 'restart', service_name]
		
		return []

@unique
class SessionFamily(Enum):
	'''Enumeration of user sessions.'''
	
	UNKNOWN = auto()
	TTY = auto()
	X11 = auto()
	WAYLAND = auto()
	MACOS = auto()
	
	@classmethod
	@cache # This is unlikely to change between runs in the same sitting.
	def fetch(cls, /) -> Self:
		'''Get the current session type.'''
		
		# This is limited to the session used to login to the machine.
		session_type = environ.get('XDG_SESSION_TYPE', 'tty')
		
		if which('loginctl') is not None:
			from re import match
			
			version_output = run(
				['loginctl', '--version'], capture_output=True, check=True, encoding='utf-8',
			).stdout.split('\n')[0] # yapf: disable
			version = match(r'^\w+ (\d{3}) .*$', version_output)
			
			if version is not None:
				# Do not use '--output=X' here; elogind doesn't support it.
				# This comparison is needed due to a regression in v256 that doesn't support '-o json'.
				session_options = ['-j'] if int(version.group(1)) >= 256 else ['-o', 'json'] # noqa: PLR2004
				
				sessions_output = run(
					['loginctl', *session_options], capture_output=True, check=True, encoding='utf-8',
				) # yapf: disable
				sessions = loads(sessions_output.stdout)
				
				current_session = None
				username = environ.get('SUDO_USER') or environ.get('DOAS_USER') or getuser()
				for session in sessions:
					if session['user'] == username:
						# list is sorted already, so fetch the session with lowest id; the first one.
						current_session = session['session']
						break
				
				if current_session is not None:
					session_type = run(
						['loginctl', 'show-session', current_session, '--property=Type'],
						capture_output=True, check=True, encoding='utf-8',
					).stdout.strip().split('=')[-1] # yapf: disable
		
		ret = cls.TTY
		
		if DistroFamily.fetch() == DistroFamily.MACOS:
			ret = cls.MACOS
		
		elif session_type == 'x11':
			ret = cls.X11
		
		elif session_type == 'wayland':
			ret = cls.WAYLAND
		
		return ret # type: ignore[return-value]
	
	@classmethod
	def is_running_gui(cls, /) -> bool:
		'''Find out if the current session is running a GUI.'''
		
		return cls.fetch() not in (cls.UNKNOWN, cls.TTY)

# This _cannot_ use @unique.
class LibcFamily(Enum):
	'''Enumeration of libc options.'''
	
	UNKNOWN = auto()
	DARWIN = auto()
	GNU = auto()
	GLIBC = GNU
	MUSL = auto()
	MSVC = auto()
	
	@classmethod
	@cache # This assumes that there is only one relevant libc on ths local system.
	def fetch(cls, /) -> Self:
		'''Get the local libc implementation flavor.'''
		
		distro = DistroFamily.fetch()
		
		if distro.is_windows():
			return cls.MSVC # type: ignore[return-value]
		
		if distro is DistroFamily.MACOS:
			return cls.DARWIN # type: ignore[return-value]
		
		ldd_cmd = ['ldd', '--version']
		if which(ldd_cmd[0]) is not None:
			from re import IGNORECASE, search
			from subprocess import PIPE, STDOUT
			
			ldd_output = run(ldd_cmd, stdout=PIPE, stderr=STDOUT, encoding='utf-8', check=True)
			version = search(r'(musl|gnu|glibc)', ldd_output.stdout.split('\n')[0], IGNORECASE)
			
			if version is not None:
				try:
					return cls[version.group(1).upper()]
				
				except KeyError:
					# This gets handled below.
					pass
		
		return cls.UNKNOWN # type: ignore[return-value]

if __name__ == '__main__':
	# Simplify interactive testing.
	DF = DistroFamily
	IF = InitFamily
	SF = SessionFamily
	LF = LibcFamily

# vim: set ts=4 sw=0 sts=-1 noet ai sta:
